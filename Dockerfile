FROM node:12

WORKDIR /home/node

COPY package*.json ./

RUN npm install -g nodemon

RUN npm install



COPY . .

EXPOSE 8080
CMD [ "nodemon", "app.js" ]