const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const MongoConnect = (callback) => {
  MongoClient.connect('mongodb://root:root@localhost:27017', { useUnifiedTopology: true})
    .then(client => {
      console.log('connected');
      _db = client.db('shop');

      callback();
    })
    .catch(err => console.log(err));
}

const getDb = () => {
  if (_db) {
    return _db;
  }

  throw 'No database found!';
}

exports.MongoConnect = MongoConnect;
exports.getDb = getDb;