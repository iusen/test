const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');

const User = require('../models/user');

const transporter = nodemailer.createTransport(sendgridTransport(
  {
    auth: {
      api_key: process.env.SEND_GRID_API_KEY
    }
  }
))

exports.getLogin = (req, res, next) => {
  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login',
  });
};

exports.getSignup = (req, res, next) => {
  res.render('auth/signup', {
    path: '/signup',
    pageTitle: 'Signup',
    isAuthenticated: false
  });
};

exports.postLogin = (req, res, next) => {
  User.findOne({email: req.body.email})
    .then(user => {
      if (!user) {
        req.flash('error', 'Email or password is invalid.')
        return res.redirect('/login');
      }

      bcrypt
        .compare(req.body.password, user.password)
        .then(doMatch => {
          if (doMatch) {
            req.session.isLoggedIn = true;
            req.session.user = user;
            return req.session.save(err => {
              return res.redirect('/');
            });
          }
          req.flash('error', 'Email or password is invalid.')
          return res.redirect('/login');
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err));
};

exports.postSignup = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;

  User
    .findOne({email: email})
    .then(user => {
      if (user) {
        req.flash('error', 'Email already used.')
        return res.redirect('/signup');
      }

      return bcrypt
      .hash(password, 12)
      .then((password) => {
        return new User({
          email: req.body.email,
          password: password,
          cart: {
            items: []
          }
        })
          .save();
      })
      .then(() => {
        res.redirect('/');
        transporter.sendMail({
          to: email,
          from: 'usenko.illya@gmail.com',
          subject: 'Signup succeeded!',
          html: '<h1>You successfully signed up!</h1>'
        })
      })
    })
    
    .catch(err => console.log(err));
};

exports.postLogout = (req, res, next) => {
  req.session.destroy(err => {
    console.log(err);
    res.redirect('/');
  });
};
